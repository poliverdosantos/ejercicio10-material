import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';
import { ListaValoresI } from '../interface/lista_valores';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
// :
  valores:ListaValoresI[] =[]

  constructor(private servicioUsuario: UsuarioService, private router:Router) { }

  ngOnInit(): void {
    this.servicioUsuario.obtenerValor().subscribe({
      next: user => {
        console.log(user);
         this.valores = user;
        // console.log(user['drinks']);
        
      },
      error: error => {
        console.log(error);
      },
    })
  }

}
