import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ListaValoresI } from '../components/interface/lista_valores';


@Injectable({
  providedIn: 'root'
})

export class UsuarioService {
url= 'https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list';
//url='https://api.chucknorris.io/jokes/random'
  constructor(public  http:HttpClient) { }


  obtenerValor(): Observable<ListaValoresI[]>{
    let direccion = this.url + "drinks/"
    return this.http.get<ListaValoresI[]>(this.url);
  }

}
